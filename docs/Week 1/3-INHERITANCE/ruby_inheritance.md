#  Class, Modules and Mixin
To inherit is to acquire from somewhwere, you can inherit you mother looks,
your fathers strictness which maybe  he is inherited from is from father before
him.


Inheritance is a kind of downward-chaining relationship between two classes (the super-class and the subclass), whereby one class “inherits” from another and the instances ofthe subclass acquire the behaviors—the methods—defined in the superclass. 
## Inheritance in classes
A class acquire propeties of other class, the who is the owner of these traits is known as `super class` while the one inheriting the properties(methods) is
known as `sub class`.Think of super class  as your father and subclass as you.

Let say we are classifying living things, we put all animals together in a class `Animals` and have a class like so:

``` ruby
class Animal
	def reproduction
 		"They reproduce"
  	end
	def motion
		"capable of motion"
	end

end
```
Create a  file *classification.rb* and add the code above.

Let us say our classification has gone deep down and now we are at point where
the we have to group traits that are similar together and octopus call this grouping octopus.These traits were:
- reproduce
- motion
- swim
Our class(group) would be like so:

``` ruby
class Octopus
	def reproduction
 		"They reproduce"
  	end
	def motion
		"capable of motion"
	end
	def swim
		"can swim"
	end


end
```



You realize that the traits of Octupus duplicate traits of Animals.In spirit of Keeping
 code  dry and code reuse, is not what we want.Since Octopu  is an animal it has traits
of Animal and can inherit from it like so:

 

Let's use edit *classification.rb* by adding class Dog like

```ruby
class Animal
	...
	...
end

class Octopus<Animal
	def swim
		"can swim"
	end

end

shaggy = Octopus.new
puts shaggy.reproduce
```
```shell
$ ruby animal.rb
$ can reproduce
```

When we ask shaggy(Octopus)instance of  wether it know reproduces it respond graciously

The Class Octopus has the behavors of the animal that is the power of inheritance.
### Overiding methods 
Lets our animal is capable of dying.So let edit *classification.rb* to that method.

```ruby


class Animal
	...
	...
	def die
	 "can die naturally"
	end
end
```

What of Octopus, by the way this is fun a fact octupus do not die natural death.When 
we ask octupus to die it "says die naturally" but that is not true.From fun fact 
we realized that Octopus do not die naturally.So what we do worry not ruby got you
covered.We can change response of Octopus.This is known as `method over ride`.Let us do that.

```ruby


class Animal
	...
	...
	def die
	 "can die naturally"
	end
end	
class Octopus < Animal
	def die
	"do not die naturally"
	end

end

shaggy = Octupus.new
shaggy.die
```

```shell
$ ruby classification.rb
$ do not die naturally
```
#### Exercise

- create a class `CentralGovernment` with methods `taxes`, `corrects`, ` enforce_laws`(should have default parameter of value, "Fairly"), depending on your view on central government, define the above
methods with return value being how you central government perform them.

- Create another class `CountyGovernment` that inherits from class `CentralGovernment`. 
- `CountyGovernment` should not enforce national laws override return
- value of `enforce_laws` to be "Should only enforce county laws" in `CountGovernment`class.

## Modules

Lets think for about our classification for a minute Octupus is  an Animals and Animal is LivingThing.Therefore
we can say class LivingThing  is super class of class Animals.Let us create class LivingThings like so:
edit *classifaiction.rb*
```ruby
class LivingThing
	
	def survival
		"Adaptation through evolution"
	end

end
class Animal 
...
...
end
class Octopus<Animal
...
end
```

Since inheritance is down chain class Animal could inherit from LivingThing which in turn inherits and Octupus could inherit from
Animal therefore having traits of LivingThing in the process.But what if  we wanted to multi-inheritance, that is Octopus inheriting
from Animal and LivingThing and the same time like so:

```ruby
class LivingThing
...
end
class Animal
...
..
end
class < Animal < LivingThing
...
end
```

Well above code throws an error, multi-inheritance is not possible in class in ruby, so what do we do that where modules come in.

```ruby
Module LivingThing
	def survival
		"Adaptation through evolution"
	end
end

class Animal 
	def motion
		"capable of motion"
	end
	def swim
		"can swim"
	end
	def die
	"can die naturally"
	end
end

class Octopus < Animal
	include LivingThing
	def die
	"do not die naturally"
	end
end
```
Now Octupus  have all traits of LivingThing  try asking  an instance of  Octopus if it respond to  
 swim

The core difference between a module it allows multiple inheritance while class do not.
On other hand while class you can create instance(that is object) you cannot do so with module





