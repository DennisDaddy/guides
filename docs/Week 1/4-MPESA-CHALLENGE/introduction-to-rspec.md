# Introduction and Setting Up

What is tdd, imagine a situation where are cooking it entails using different spices 
salts, onions and sugar am not sure what is end goal, am not a good cook after all but 
you get the point we are cooking, put the salt taste, or should i say testing
 if it is enough, the same applies to amount sugar and  so  on your target making meal 
which default free if may use the phrase.So thats what tdd is all about  testing as you
developing.

There several libraries or gems which helps in test driven development in ruby,
but use rspec feel free to explore others.So what exactly is a gem or library you
may ask lets take the cooking anology, you how

## Introduction to Rspec

First install rspec using command `gem install rspec`. 

For more check [rspec docs](https://github.com/rspec/rspec)

```
$mkdir bank 
$cd bank
$ rspec --init
$ create   .rspec
$ create   spec/spec_helper.rb
```

We create a directory `bank` where our project will live, the we initialize rspec using `rspec --init` creating `.rspec` and `spec_helper.rb`, .rspec is well we put our setting 
will create our spec file inside spec folder this not necessary but it is a the common convention.

```
$ cd spec
$ touch account_spec.rb
```
Note _spec format of our i.e account_spec.rb.
This is where is our test our lives. Add the following code 
on file account_spec.rb

*account_spec.rb 1*
```
require './lib/account.rb'

    describe Account do
        it "verify amount deposit to accurate" do
            account1 = Account.new
            deposit = account.deposit(1000)
            expect(deposit).to eq 1000
        end
    end

```
```
$ rspec
$ An error occurred while loading ./spec/account_spec.rb.
Failure/Error: require '.lib/account.rb'

LoadError:
  cannot load such file -- .lib/account.rb
# ./spec/account_spec.rb:1:in `<top (required)>'
No examples found.


Finished in 0.00018 seconds (files took 0.09744 seconds to load)
0 examples, 0 failures, 1 error occurred outside of examples

```
There is an error yep we yet to create the file `account.rb` lets do just that .
Lets create a directory `lib` in the root directory(bank) and creat account.rb file and rerun the test

```
$mkdir lib && touch lib/account.rb
$rspec

An error occurred while loading ./spec/account_spec.rb.
Failure/Error:
      describe Account do
          it "verify amount deposit to accurate" do
              account1 = Account.new
              deposit = account.deposit(1000)
              expect(deposit).to eq 1000
          end
  end

NameError:
  uninitialized constant Account
# ./spec/account_spec.rb:3:in `<top (required)>'
No examples found.


Finished in 0.0002 seconds (files took 0.10203 seconds to load)
0 examples, 0 failures, 1 error occurred outside of examples


```
Take  note on the where the error is 

```
NameError:
  uninitialized constant Account
# ./spec/account_spec.rb:3:in `<top (required)>'
No examples found.
```
This give a hint that we need to create either a class Account or module but since it involve instance it will be class.We give test what it wants according to failure.
So let to that remember our aim is to make the test pass and refactor later.

*account.rb 1*

```
class Account
end
```
run the test again using rspec command and the error we be like so(check verbose form on your terminal)
```
Failure/Error: deposit = account1.deposit(1000)
     
     NoMethodError:
       undefined method `deposit' for #<Account:0x000055eae7874510>


```

It is important to note is `1 example, 1 failure` our tests are called example, try finding out why.

So lets deal with our failure by creating method deposit like so:
*account.rb 2*
```ruby
class Account 
  def deposit
  end
end
```

You know the drill run 
```shell
$ rspec
```

try figuring out the error uh!! our deposit is expecting a parameter and we gave none.lets fix that..

*account.rb 3*

```ruby
class Account 
  def deposit(amount)
  end
end
```
this is tdd friend we make changes the run the test do that hopefully you know how and am proud of you for that.

there is a failure we are expecting `1000` we got `nil`.I wonder how that come to be..Oop method returns what you tell to return, so `deposit` return obediently return `nil` honestly that what you told it to do return.How do fix that,it
is the easiest thing you done so far give it return value of `1000`.
*account.rb 4*

```ruby

class Account 
  def deposit(amount)
   1000
  end
end
```
We made a change our rule remains the same, you guess it right the test.
Boom!!! Jackpot trust that is happiest of programmer's life tests are passing everything is  `green`.

Now come the refactor i.e perfect our code what if the deposit was 2000.what happens ok try giving our method `2000`.
*account.rb 5 *

```ruby
class Account 
  def deposit(amount)
   2000
  end
end
```
Run the test and the happen moment is gone our testing expect `1000` but what is getting is `2000`.So what the best foward,
when amount deposit will actually our deposit therefore deposit method should return amount like so:

```ruby
class Account 
 def deposit(amount)
    amount
 end
end
```
Run the test `green` again.

Take notes of these activities we did:
- We first wrote the test and never changed but rather 
changed our code to make test pass
- Upon passing we refactored our code with confindence 
- Then made it pass again
### use tdd to solve this

1. Modify our code  to use attr_accessors instead of explictly defining deposit method
2. Account  to have a name and the name must not be longer 8 characters, remove blanks (blank in this case include a 
  tab or spacebar input),if it surpasses 8 character it should warn us with message `account name should at most 8 character` if it is empty it display an error message should `account name can't be blank`.



