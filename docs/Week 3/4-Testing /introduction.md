# Introduction To Jasmine

Jasmine is one of the testing framework for javascript.It supports many language but in this respect
we are using for javascript testing.

### Setting up 

Following setup guide in the beginning of the week section. Set up jasmine globally.Create a directory I adviced you name it Calculator.Inside the Calculator setup package.json file by running `npm init` and answer the questions or just enter to the end.You can install jasmine  in your local `package.json` using command `npm install --save-dev jasmine` 
file  With your working directory as Calculator run command  `jasmine init`

to initialize jasmine.List the content of working directory.You should see node_module and spec directories.


We are going to create a calculator which does 4 operations:

 - add

 - subract

 - multilpy

 - divide

## Starting Our test 

Create a file `calcultor_spec.js` inside the spec directory.

add the following as its content.We will  put our test and implementation in a sinlge file that is
`calcultor_spec.js`. 
 
*calcultor_spec.js*

```js
## Our implemenation



## Our test
describe("Calculator",function(){
	it("The function should add 2 numbers",function() {
			
	expect(add(5,6)).toBe(11);
	});
});

```

The syntax is more like rspec which  you have already covered .
Describe block is used to put in a block tests that are related in this case calculator.It keyword denotes were our tests lives.We mock our expectation i.e we expect a function named addNumber given to 
value to return 11.[look at others jasmine mocks and matchers](https://jasmine.github.io/pages/docs_home.html)


run this on the shell

```shell
$jasmine
  Message:
    ReferenceError: add is not defined 

```
The  add is not defined that is our error let us deal with it by the function like so:

*calculator_spec.js*

```js
## implementation
function add(){
	
}

## Our test
...

```

run test again

```shell
$ jasmine
 Message:
    Expected undefined to be 11.


``` 

We got  yet 11 the easiest way to make that  pass is to return 11 as our ret
urn value.So let do just that

*calculator_spec.js*

```js
## our implementation
function add(){
	return 11
}

## Our test
...

``` 
run the test again, it passes well we wouIld we are done but I got a question what if we are adding
20 and 20.The function should return 40 but instead in returns 11.Let us put that in test and see.Sometimes
one test do not much of we need doing more ensure accuracy.

So let add our idea into the test like so:

```js
## our implementation

## Our test
describe("Calculator",function(){
	it("The function should add 5 and 6 numbers",function() {
			
	expect(sum(5,6)).toBe(11);
	});
	it("The function should add 20 and 20 numbers",function() {
			
	expect(sum(20,20)).toBe(40);
	});
});

```
Run test again this time round there is a failure `Expected 11 to be 40.`. How do we solve and make it
pass our function expects two arguements.Therefore in its definition we should give it two parameter and
find their sum.


```js
## Our implementation

function add(a,b){
	return a+b
}

## our test
...

```

## Exercise

Using jasmine as your test framework with test driven developmen as your principle.Give the calculator ability to

- subtract

- divide

- multiply


