# Persisting In an Organised Manner Using Database

Database help us store our data in 

- [sqlite3](https://rubygems.org/gems/sqlite3/versions/1.3.11)

- [dm-sqlite-adapter](https://rubygems.org/gems/dm-sqlite-adapter/versions/1.2.0)

-[datamapper](https://datamapper.org/docs/)


### Setting up Gemfile

Our dependencies are becoming more and sometime conflict may arise if using our application require dependencies whose version conflict with what is in our computer to be on the safe side, we create a `Gemfile` which contains all our project depedencies.Create a `Gemfile` on the root of our project and update its content like so:

*Gemfile*
```ruby
source 'https://rubygems.org'

gem 'sinatra',            '1.4.8'
gem 'data_mapper',        '1.2.0'
gem 'dm-sqlite-adapter',  '1.2.0'
```

Update content of config.ru include `bundler` like so:

*config.ru*
```ruby
require 'rubygems'
require 'bundler'

Bundler.require
require "./myapp"
run Sinatra::Application
```
bundler is an application use to install ruby dependecies.For more on [bundler](https://github.com/bundler/bundler)

Run bundle install at the root of your application. likes

```shell
bundle install
```

Incase you run into bundler not available issue kindle install it using above from instruction on the above.

## setting up the db and our model
Let us our db like so:

*myapp.rb*

```ruby
## after requiring modules
configure :development do
    DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/myapp_development.db")
end
class Calculation
  include DataMapper::Resource  
  property :id,     Serial
  property :first_number,      Integer
  property :second_number,      Integer
  property :sum,      Integer  
  property :created_at,   DateTime
  property :update_at, DateTime
end 
DataMapper.finalize
DataMapper.auto_migrate!

##routes
..
```
let's modify our route `/sum` to create our calculation results

```ruby
get '/sum/new' do
    erb:sum // replace this with new i.e erb:new
  end

  post '/sum/create' do # replace post 
    first_number =params[:first_number].strip
    second_number =params[:second_number].strip 
    sum = first_number.to_f + second_number.to_f    
    result = Calculation.create(:first_number => first_number, :second_number => second_number, :sum => sum) 
    id = result.id
    redirect "/sum/show?id=#{id}"
end  
    


```
Note the modification of `/sum` to `/sum/new` this common convention when render form to create a resource.On replace sum with new also rename sum tempalte to new template.
The show(show is use to render only single content in this case only one result that which we have created) `/sum/show` do not exists yet so let us create it. Redirect is used to mean you requesting for something I don't have it so here in this case `/sum/show:id` that is the content we will render.The reason for using id it uniquely identifies a record.Modidify content of `/calculation_result` with like so:

*myapp.rb*

```ruby
get '/sum/show:id' do
    @sum = Calculation.find(id)
    erb:show
end
```
We find our record for the database the display it.
Create `show.erb` template and add content of `calculation_results.erb` eto it. Now delete `calculation_results.erb` we do not need it.

Try visiting `localhost:9292/sum/new` the create a new calculation

Now let us create ability to display all record from the database

Our route for displaying all `results` was `all_results` in keeeping with convention  let  us rename it `/sum/index`

*myapp.rb*

```ruby
##other codes
get `sum/index` do # "/all_results" renamed
    @results = Calculation.all
    erb:index
end
# other codes
```
We get all existing results from our database and display them.Create and `index.erb` whose content is like so:

```html
<h1> All Results</h1>
<table>
  <thead>
    <tr>
      <th>First Number</th>
      <th>Second Number</th>
      <th>Sum</th>
    </tr>
  </thead>
 
  <tbody>
    <% @results.each do |result| %>
      <tr>
        <td><%= result.first_number%></td>
        <td><%= result.second_number %></td>
        <td><%= result.sum %></td>        
      </tr>
    <% end %>
  </tbody>
</table>

```
Note the modification of get and post `/sum` to `/sum/new`  and `/sum/create` this common convention when render form to create a resource.On replace sum with new also rename sum tempalte to new template.
The show(show is use to render only single content in this case only one result that which we have created) `/sum/show` do not exists yet so let us create it. Redirect is used to mean you requesting for something I don't have it so here in this case `sum/show` with params, that is the content we will render.The reason for using id it uniquely identifies a record.Modidify content of `/calculation_result` with like so:

*myapp.rb*

```ruby
get '/sum/show' do
     id = params[:id]
    @result= Calculation.get(id)
    erb:show
end
```
We find our record for the database the display it.
Rename `calculation_results.erb` to `show.erb`  . Now delete `calculation_results.erb` we do not need it.Content of show.erb  should be like so.


*show.erb*

```html
<p>First Number: <%=@result.first_number%></p>
<p>Second Number: <%=@result.second_number%></p>
<p>Sum: <%=@result.sum%></p>
```

Try visiting `localhost:9292/sum/new` the create a new calculation

Now let us create ability to display all record from the database

Our route for displaying all `results` was `all_results` in keeeping with convention  let  us rename it `/sum/index`

*myapp.rb*

```ruby
##other codes
get `sum/index` do # "/all_results" renamed
    @results = Calculation.all
    erb:index
end
# other codes
```
We get all existing results from our database and display them.Create and `index.erb` whose content is like so:

```html
<h1> All Results</h1>
<table>
  <thead>
    <tr>
      <th>First Number</th>
      <th>Second Number</th>
      <th>Sum</th>
    </tr>
  </thead>
 
  <tbody>
    <% @results.each do |result| %>
      <tr>
        <td><%= result.first_number%></td>
        <td><%= result.second_number %></td>
        <td><%= result.sum %></td>        
      </tr>
    <% end %>
  </tbody>
</table>

```


It is tiring to display our content via url in `show.erb` create a link(labelled "back") that if clicked will list display index page(index.erb) and `index.rb` create a link(labelled "show")  which if clicked shows a single result



Now visit `localhost:9292/sum/index`





`





