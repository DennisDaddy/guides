# control flow

Program flow from right toleft and top to down in normal case that is
`let x = 3` should be read as 3 assigned to x
 
To prove that let us try reading this `let x = 3 + 5`.It should be read as
sum of 5 and 3 assigned to x we evaluate the right hand side first then
left hand side.

But in some case and in most cases this normal flow needs to be interrupted
this is where control flow comes in. [More on control flow](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling)



If the calculator shows a non-zero number, we want to append the clicked key to the displayed number. To append a number, we concatenate a string.

String concatenate is common technique of joining two or more strings together.
ie `"jane" + "doe" = "janedoe"`

Let us edit our *js* file like so:

```js
if (!action) {
  if (displayedNum === '0') {
    display.textContent = keyContent
  } else {
    display.textContent = displayedNum + keyContent
  }
}
```

By now you can to *devtool* and see the impact of your work.Try pressing 
numbers on the calcutor.
