 
# Introduction to Sass

## what is sass?

It comes from the words 'Syntactically Awesome Stylesheets',
remember CSS is called Cascaded Stylesheets so SASS tries to make it more awesome than it currently is
From SASS website: CSS on its own can be fun, but stylesheets are getting larger, more complex,
and harder to maintain. This is where a preprocessor can help. Sass lets you
use features that don't exist in CSS yet like variables, nesting, mixins,
inheritance and other nifty goodies that make writing CSS fun again

## Variables

   Think of variables as a way to store information that you want to reuse
   throughout your stylesheet. You can store things like colours, font stacks, or
   any CSS value you think you'll want to reuse. Sass uses the $ symbol to make
   something a variable.
  ```css
  $font-stack:    Helvetica, sans-serif;
  $primary-color: #333;

  body {
    font: 100% $font-stack;
    color: $primary-color;
  }
  ```

   When SASS is processed it takes the variables and replaces the variable names
   with CSS values defined for them. The final output for the above will be:
```css
  body {
    font: 100% Helvetica, sans-serif;
    color: #333;
  }
```

## Nesting

   Sass will let you nest your CSS selectors in a way that follows the same
   visual hierarchy of your HTML. Do not however nest the children so much
   because this will result in maintenance nightmares

```css
    nav {
      ul {
        margin: 0;
        padding: 0;
        list-style: none;
      }

      li { display: inline-block; }

      a {
        display: block;
        padding: 6px 12px;
        text-decoration: none;
      }
  }

```
   The code above compiles to:
```css
      nav ul {
        margin: 0;
        padding: 0;
        list-style: none;
      }

      nav li {
        display: inline-block;
      }

      nav a {
        display: block;
        padding: 6px 12px;
        text-decoration: none;
      }

```

## Partials

These are SASS files that can be included into other SASS files. It helps us
modularize our code into separate files. Names starts with a leading
underscore `_`
These files are added to other files by using SASS   `@import` functionality


### Lets do some practice

Go to your code directory and create a file called ` trying.scss `. Create another partial called `_mypartial.scss`

Inside `trying.scss` have the following:

```css
    @import 'mypartial';
    $footer-background: #ccc;
    $white: #ffe;

    .footer {
      background: $footer-background;
      padding: 2rem 0 2rem;
      color: $white;

      .email-image {
        margin-left: 1rem;
        padding-top: 0.75rem;
        h2 {
          height: 2em;
        }
      }

      .logo {
        width: 3em;
        padding-bottom: 1em;
      }
    }
```

On the partial, have the following content:

```css
    .from-partial {
      color: red;
    }

```

### Getting the output 

In order for us to see the output of the file above, we need to have sass    installed in our computers. Command below installs it for us

Ensure you nodejs and node package manager(npm) before run this command

node comes with npm but you need to update npm

- [Node install](https://nodejs.org/en/download/)

- [upgrade npm](https://www.npmjs.com/get-npm)

 
```shell
$ npm i -g sass
```


Make sure you are in the same directory as the ~trying.scss~ file then
    
```shell
$ sass trying.scss
```
   

This will print the output to our terminal. Have a look at the output if it
is what you expected to see, if you get errors, try to correct them without
earching the internet. Ask for a trainer's help if you cannot move

Last step: lets write our changes to a file instead of the terminal

```shell
    $ sass trying.scss app.css
```

Open the `app.css` file and look at the content