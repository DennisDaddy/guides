# Event listners and Event handler

The first steps to building this calculator are to be able to (1) listen for all keypresses and (2) determine the type of key that is pressed. In this case, we can use an event delegation pattern to listen, since keys are all children of .calculator__keys.

An event is something that happens when user interact with the web page, such as when he clicked a link or button, entered text into an input box or textarea, made selection in a select box, pressed key on the keyboard, moved the mouse pointer, submits a form, etc. In some cases, the Browser itself can trigger the events, such as the page load and unload events.

When an event occur, you can use a JavaScript event handler (or an event listener) to detect them and perform specific task or set of tasks. By convention, the names for event handlers always begin with the word "on", so an event handler for the click event is called **onclick**, similarly an event handler for the load event is called onload, event handler for the blur event is called **onblur**, and so on.

There are several ways to assign an event handler. The simplest way is to add them directly to the start tag of the HTML elements using the special event-handler attributes. For example, to assign a click handler for a button element, we can use onclick attribute, like this:

`<button type="button" onclick="alert('Hello World!')">Click Me</button>`

You can also create your own function and use like so:

```html
<button type="button" id="myBtn">Click Me</button>
<script>
    function sayHello() {
        alert('Hello World!');
    }
    document.getElementById("myBtn").onclick = sayHello;
</script>

The above code listens to when button Click Me is clicked is show a pop up
"Hello World" on browser.
```
On you *calculator.js* file  add the code below

```js
const calculator = document.querySelector(‘.calculator’)
const keys = calculator.querySelector(‘.calculator__keys’)
keys.addEventListener(‘click’, e => {
 if (e.target.matches(‘button’)) {
   // Do something
 }
})
```

The above code use `ES6`   syntax for creating functions(arrow function).
That is
```
e => {
 if (e.target.matches(‘button’)) {
   // Do something
 }
```

Arrow functions are what in programming called lambda or anonymous function.You met them in ruby we called them `blocks` over there.
General syntax is like so:
```js
(argument1, argument2, ... argumentN) => {
  // function body
}
```

You can always assign them to variable for reuse like so:
```js
const func = (argument1, argument2, ... argumentN) => {
  // function body
}
```
methods in ruby  they are basically the same thing. Just different language

Next, we can use the data-action attribute to determine the type of key that is clicked.

In your *js* file just below ` const keys = calculator.querySelector(‘.calculator__keys’` 




In your *js* file remove //Do Something comment and the following code.


```js
const key = e.target
const action = key.dataset.action
if (!action) {
  console.log('number key!')
}
```
